var express 	= require('express'),
	config 		= require("./configs"),
	bodyparser 	= require('body-parser'),
	session 	= require('express-session'),
	bourne 	= require('bourne'),
	cors 		= require('cors'),
	mongodb 	= require('mongoskin'),
	jwt 		= require('jwt-simple'),
	_ 		= require('underscore'),
	crypto 		= require('crypto'),
	app 		= express(),
	db 		= mongodb.db(config.mongodb.brgydata, {native_parser:true});

var router 		= express.Router();

function hash (password) {
	return crypto.createHash('sha256').update(password).digest('hex');
};

var tokens = [];

function requiresAuthentication(req, res, next) {
    if (req.headers.access_token) {
        var token = req.headers.access_token;
        if (_.where(tokens, token).length > 0) {
            var decodedToken = jwt.decode(token, app.get('jwtTokenSecret'));
            if (new Date(decodedToken.expires) > new Date()) {
                next();
                return;
            } else if(!req.user){
                removeFromTokens();
                res.redirect('/logout');
            }
        }
    }
    res.redirect('/logout');
};

function removeFromTokens(token) {
    for (var counter = 0; counter < tokens.length; counter++) {
        if (tokens[counter] === token) {
            tokens.splice(counter, 1);
            break;
        }
    }
}

var hour = 3600000;

app.set('jwtTokenSecret', '123456ABCDEF');
router
.use(cors())
.use(bodyparser.urlencoded({extended: true}))
.use(bodyparser.json({extended: true}))
.use(session({
			  secret  : 'sdfsdf46fgfhy45fffed5jkfdfszzvm890ffdhdj',
		      maxAge  : new Date(Date.now() + 3600000), //1 Hour
    		  expires : new Date(Date.now() + 3600000), //1 Hour
    		  resave  : true,
    		  saveUninitialized: true
    		  // cookie: {expires:  new Date(Date.now() + 3600000)} //will expire in 1 hour including the server
		    }))

.post('/login', function (req, res){

	var user = {
		username : req.body.username,
		password : hash(req.body.password)
	};

	var adminrole = req.body.adminrole;

	function tokenfunction(data){

	       var expires = new Date();
	        expires.setDate((new Date()).getDate() + 1);
	        var token = jwt.encode({
	            username: data.username,
	            expires: expires
	        }, app.get('jwtTokenSecret'));

	        tokens.push(token);
	        data.access_token = token;
	        res.send(200, data);

	};

	if(adminrole == null){
		res.send(301, { message: 'Please choose the access type.'});
	}else{

		if(adminrole === 'provincecontrol'){

			db.collection('province_userlist').findOne(user, function (err, data){

				if(data){

					if (data.active === 'Active'){

						req.session.usercurrentId 	= data._id;
						req.session.role 		= data.role;
						req.session.username 		= data.username;
						req.session.access 		= data.access;
						req.session.province 		= data.province;
						req.session.region 		= data.region;
						req.session.accesscontrol 	= data.accesscontrol;
						req.session.themes 		= data.themes;

						tokenfunction(data);

					}else{

						res.send(401, { message: 'Account was Deactivated, contact the administrator to activate.'});
					}


				}else{
					res.send(401, { message: 'Username/password is incorrect! Please try again.' });
				}
			});

		}else if(adminrole === 'municipalitycontrol'){

			db.collection('userlist').findOne(user, function (err, data){

				if(data){

					if (data.active === 'Active'){

						req.session.usercurrentId 	= data._id;
						req.session.role 		= data.role;
						req.session.createdbyId 	= data.createdbyId;
						req.session.municipality 	= data.municipality;
						req.session.username 		= data.username;
						req.session.access 		= data.access;
						req.session.province 		= data.province;
						req.session.region 		= data.region;
						req.session.accesscontrol 	= data.accesscontrol;
						req.session.themes 		= data.themes;

						tokenfunction(data);

					}else{

						res.send(401, { message: 'Account was Deactivated, contact the administrator to activate.'});
					}


				}else{
					res.send(401, { message: 'Username/password is incorrect! Please try again.' });
				}
			});
		}else if(adminrole === 'barangayaccesscontrol'){

			db.collection('brgy_userlist').findOne(user, function (err, data){

				if(data){

					if (data.active === 'Active'){

						req.session.usercurrentId 	= data._id;
						req.session.role 		= data.role;
						req.session.createdbyId 	= data.createdbyId;
						req.session.municipality 	= data.municipality;
						req.session.username 		= data.username;
						req.session.access 		= data.access;
						req.session.province 		= data.province;
						req.session.region 		= data.region;
						req.session.barangay 		= data.barangay;
						req.session.accesscontrol 	= data.accesscontrol;
						req.session.themes 		= data.themes;
						req.session.captain 		= data.captain;
						req.session.sk_chairman 	= data.sk_chairman;
						req.session.kagawad 		= data.kagawad;
						req.session.treasurer 		= data.treasurer;
						req.session.secretary 		= data.secretary;

						tokenfunction(data);

					}else{

						res.send(401, { message: 'Account was Deactivated, contact the administrator to activate.'});
					}

				}else{
					res.send(401, { message: 'Username/password is incorrect! Please try again.' });
				}
			});
		}else if(adminrole === 'regionadmin'){

			db.collection('userlist').findOne(user, function (err, data){

				if(data){

						req.session.usercurrentId 	= data._id;
						req.session.role 		= data.role;
						req.session.username 		= data.username;
						req.session.access 		= data.access;
						req.session.accesscontrol 	= data.accesscontrol;
						req.session.themes 		= data.themes;
						req.session.region 		= data.region;

						tokenfunction(data);

				}else{
					res.send(401, { message: 'Username/password is incorrect! Please try again.' });
				}
			});
		}else if(adminrole === 'Viewer'){

			db.collection('viewer_userlist').findOne(user, function (err, data){

				if(data){

					req.session.usercurrentId 	= data._id;
					req.session.username 		= data.username;
					req.session.accesscontrol 	= data.accesscontrol;
					req.session.barangay 		= data.barangay;
					tokenfunction(data);

				}else{
					res.send(401, { message: 'Username/password is incorrect! Please try again.' });
				}
			});
		}
	}

});

router
.use(cors())
.use(bodyparser.urlencoded({extended: true}))
.use(bodyparser.json({extended: true}))
.post('/logout', function (req, res){

		req.session.usercurrentId   = null;
		req.session.role                  = null;
		req.session.createdbyId     = null;
		req.session.municipality    = null;
		req.session.barangay 	     = null;
		req.session.username 	     = null;
		req.session.username 	     = null;
		req.session.access 	     = null;
		req.session.province 	     = null;
		req.session.region 	     = null;
		req.session.accesscontrol  = null;
		req.session.themes  	     = null;
		req.session.captain 	     = null;
		req.session.sk_chairman    = null;
		req.session.kagawad 	     = null;
		req.session.treasurer 	     = null;
		req.session.secretary 	     = null;


		var token= req.headers.access_token;
	    removeFromTokens(token);
	    res.send(200);

})

.use(function (req, res, next){

	if(req.session.usercurrentId){

			req.user 		= req.session.usercurrentId;
			req.currentrole 	= req.session.role;
			req.currentIdref 	= req.session.createdbyId;
			req.municipality 	= req.session.municipality;
			req.barangay 		= req.session.barangay;
			req.byusername 	= req.session.username;
			req.adminaccess 	= req.session.access;
			req.province 		= req.session.province;
			req.region 		= req.session.region;
			req.accesscontrol 	= req.session.accesscontrol;
			req.themes   		= req.session.themes;
			req.captain		= req.session.captain
			req.sk_chairman 	= req.session.sk_chairman;
			req.kagawad 		= req.session.kagawad;
			req.treasurer 		= req.session.treasurer;
			req.secretary		= req.session.secretary;


	}

	next();

});

module.exports = router;