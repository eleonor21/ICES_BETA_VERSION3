var express         = require('express'),
    config          = require("./configs"),
    bodyparser      = require('body-parser'),
    mongodb         = require('mongoskin'),
    cors            = require('cors'),
    session         = require('express-session'),
    db              = mongodb.db(config.mongodb.brgydata, {native_parser:true});
    router          = express.Router(),
    uuid            = require('node-uuid'),
    methodOverride  = require('method-override');

//=========================================================================
//Queries Routes
//=========================================================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getfullname/:fullname')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({fullname: req.params.fullname, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({fullname: req.params.fullname, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({fullname: req.params.fullname, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({fullname: req.params.fullname, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({fullname: req.params.fullname}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({fullname: req.params.fullname, province : req.province}).toArray(function (err, data){
            res.json(data);

        });
    }
    });

    //==================================
    // Get Blood Type
    //==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getbloodtype/:bloodtype')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({'blood_type.blood_type': req.params.bloodtype, province : req.province}).toArray(function (err, data){
            res.json(data);

        });
    }
});

//==================================
// Get healtha vailed Type
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/healthavailed/:health')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({healths: req.params.health, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({healths: req.params.health, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({healths: req.params.health, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({healths: req.params.health, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({healths: req.params.health}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({healths: req.params.health, province : req.province}).toArray(function (err, data){
            res.json(data);

        });
    }
});

//==================================
// Get the occupation
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getoccupation/:occuaption')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, 'barangay._id' : req.barangay._id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'provincecontrol'){

        db.collection('brgyresident').find({prof_occupation: req.params.occuaption, province : req.province}).toArray(function (err, data){
            res.json(data);

        });
    }
});

//==================================
// Get the report
//==================================

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .route('/getreport/:id')
    .get(function (req, res){

     if(req.accesscontrol === 'regionadmin'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, 'region.region_name': req.region}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'municipalitycontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, 'municipality.municipality_name': req.municipality.municipality_name}).toArray(function (err, data){
            res.json(data);

        });
    }else if(req.accesscontrol === 'barangayaccesscontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id, usercurrentId: req.user}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'officialaccesscontrol'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id}).toArray(function (err, data){
            res.json(data);
        });
    }else if(req.accesscontrol === 'Viewer'){

        db.collection('brgyclearance').find({'barangay._id': req.params.id}).toArray(function (err, data){
            res.json(data);
        });
    }
});

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/getspecificmonth', function (req, res){

        var report_request = {
            brgyIds : req.body.brgyIDs,
            req_month : req.body.getmonth
        };

        db.collection('brgyclearance').find({'barangay._id': report_request.brgyIds, specificmonth: report_request.req_month}).toArray(function (err, data){
            res.json(data);
        })

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetSpecificyear', function (req, res){

        var report_request = {
            brgyIds : req.body.brgyIDs,
            req_month : req.body.getmonth,
            req_year : req.body.yearvalue
        };
        db.collection('brgyclearance').find({'barangay._id': report_request.brgyIds, specificmonth: report_request.req_month, dateYear: report_request.req_year}).toArray(function (err, data){
            res.json(data);
        })

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetDailyreport', function (req, res){

        var report_request = {
            brgyID : req.body.brgyID,
            getmonth : req.body.month,
            getdatevalue : req.body.datevalue,
            get_issuedYear : req.body.yearofdate,
            getday : req.body.dayofvalue,
        };

        db.collection('brgyclearance').find(
            {'barangay._id': report_request.brgyID,
                                      specificmonth: report_request.getmonth,
                                      dateYear: report_request.get_issuedYear,
                                      datevalue: report_request.getdatevalue,
                                      specificday : report_request.getday
        }).toArray(function (err, data){
            res.json(data);
        })

    });

    var today = new Date()

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/GetDailyreport', function (req, res){

        var report_request = {
            brgyID : req.body.brgyID,
            getmonth : req.body.month,
            getdatevalue : req.body.datevalue,
            get_issuedYear : req.body.yearofdate,
            getday : req.body.dayofvalue,
        };

        db.collection('brgyclearance').find(
            {'barangay._id': report_request.brgyID,
                                      specificmonth: report_request.getmonth,
                                      dateYear: report_request.get_issuedYear,
                                      datevalue: report_request.getdatevalue,
                                      specificday : report_request.getday
        }).toArray(function (err, data){
            res.json(data);
        })

    });


router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .post('/getbirthtoday', function (req, res){

        var passtobirth = {
            dvalue : req.body.dvalue,
            dmonth : req.body.dmonth,
            dyear : req.body.dyear,
        };

        db.collection('brgyresident').find(
            {birthvalue: passtobirth.dvalue,
              birthmonth: passtobirth.dmonth
        }).toArray(function (err, data){
            res.json(data);
        })

    });

router
    .use(cors())
    .use(bodyparser.json({ limit: '10000mb', extended: true}))
    .use(bodyparser.urlencoded({ limit: '10000mb', extended: true}))
    .use(methodOverride())
    .get('/getIDs', function (req, res){

            db.collection('brgyresident').find({ barangay: req.barangay}).toArray(function (err, data){
            return res.json(data);
            });

    });

module.exports = router;