'use strict';

var app = angular.module('viewReportServices', []);

app.factory("ReportService",  function($q, $http, SearchBaseurl){

return{

    GetReport:function(id){
        return $http.get(SearchBaseurl+'getreport/'+id)
        .then(function(data){
            return data;
        });
     },
    GetSpecificmonth:function(data){
        return $http.post(SearchBaseurl+'getspecificmonth/', data);
     },
    GetSpecificYear:function(data){
        return $http.post(SearchBaseurl+'GetSpecificyear/', data);
     },
    GetDaily:function(data){
        return $http.post(SearchBaseurl+'GetDailyreport/', data);
     },
    getmonthreport_res:function(data){
          var deferred = $q.defer();
          var report_res=[];
          return $http.post(SearchBaseurl+'getspecificmonth/', data);
    },
    GetresidentBirthday:function(data){
        return $http.post(SearchBaseurl+'getbirthtoday/', data);
     },
    getforresidentIds:function(){
        return $http.get(SearchBaseurl+'getIDs/');
     }
}

});