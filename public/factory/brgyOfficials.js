'use strict';

var app = angular.module('brgyOfficials', []);

app.factory("BrgyOfficial",  function($http,  BrgyOfficialcollections){

return{

    CreateNewOfficials:function(data){
        return $http.post(BrgyOfficialcollections+'/brgyofficial', data);
    },
    GetAllOfficials:function(){
        return $http.get(BrgyOfficialcollections+'/brgyofficial');
     },
    GetOfficials:function(id){
        return $http.get(BrgyOfficialcollections+'/getbrgyofficial/'+id);
     },
    UpdateOfficials:function(id,data){
        return $http.put(BrgyOfficialcollections+'/getbrgyofficial/'+id, data);
    },
    DeleteOfficials:function(id){
        return $http.delete(BrgyOfficialcollections+'/getbrgyofficial/'+id);
    }

}

});