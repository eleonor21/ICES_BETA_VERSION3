'use strict';

var app = angular.module('brgyapp');

app.controller('OfficialsaccountCtrl', function($scope, ItemsServices,
  $window, $rootScope, $modal, myAccount, BaranggayServices, $http,
  Restangular, ngTableParams, $q, $filter,
  DbCollection, $location, $resource, ResidentServices, Signupviewers, ReportService){

        function getofficialsaccount(){
                $http.get(Signupviewers + '/viewers')
                .then(function(result){
                 $scope.residentofficialsaccount = result.data;
                })
        }

        getofficialsaccount();

        $scope.addofficialaccount = function(size){

            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.addOfficialsAccountCtrl,
                size: size
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });
        }

        $scope.addOfficialsAccountCtrl = function($scope, $modalInstance, $modal){

            $scope.cancel = function() {
              $modalInstance.dismiss('cancel');
            };

           $scope.registerOfficial = function () {
            $http.post(Signupviewers + '/viewers', $scope.account)
            .then(function (result) {
                    $scope.account = result.data[0];
                    getofficialsaccount();
                    $modalInstance.dismiss('cancel');
                    $rootScope.message_access = "Official acccount will now have access: '"+ $scope.account.username + "' this user can login as viewer.";

            }, function (error) {
                    $scope.error = error.data.message;
                    console.log($scope.account);
                });
            };
        }

      $scope.editofficialaccount = function(size, id){
            var modalInstance = $modal.open({
                templateUrl: '../views/officials/brgyofficialsaccountform.html',
                controller: $scope.EditOfficialsAccountCtrl,
                size: size,
                resolve: {
                  getaccount: function($http){
                      return $http.get(Signupviewers+'/viewers/'+ id);
                  }
                }
              });

              modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
                }, function () {
                  // $log.info('Modal dismissed at: ' + new Date());

                });

      }

        $scope.EditOfficialsAccountCtrl = function($modalInstance, $modal, $scope, getaccount, $http){

              $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
              };

              $scope.account = getaccount.data;

              $scope.updateofficials = function(id){
                $http.put(Signupviewers+ '/viewers/'+ id, $scope.account)
                .then(function(result){
                  $scope.resultdata = result.data;
                      getofficialsaccount();
                      $modalInstance.dismiss('cancel');
                }, function (error) {
                      $scope.error = error.data.message;
                      console.log($scope.account);
                  });
              };
        };

})