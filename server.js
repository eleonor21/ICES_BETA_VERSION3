var express 	           = require('express'),
	config 		= require("./configs"),
	api 		= require('./api'),
	users 		= require('./account'),
	viewer 		= require('./viewer'),
           search             = require('./Search'),
           officials            = require('./brgyofficial'),
	app 		= express();

app

.use(express.static('./public'))
.use(users)
.use('/api', api)
.use('/search', search)
.use('/viewer', viewer)
.use('/users', users)
.use('/brgyofficials', officials)
.get('*', function (req, res){

	res.sendfile('public/index.html');

})

.listen(config.server.port);
 console.log(config.message);
 console.log('Database URL running "' + config.mongodb.brgydata + '..."');
 console.log("Server listening on port " + config.server.port);
